﻿using System.Collections.Generic;
using UnityEngine;
using CCKit;

public class ManageInput : MonoBehaviour
{
    public static ManageInput Ins { get; private set; }

    public delegate void AxisFire1DownDel();
    public AxisFire1DownDel axisFire1Down;

    void Awake()
    {
        Ins = this;
        InputSignals.OnAxisFire1Down += OnAxisFire1Down;
    }

    void Update()
    {
        if (ManageVirtualAxis.GetAxisDown("Fire1")) {
            if (axisFire1Down != null)
                axisFire1Down();
        }
        else if (Input.GetMouseButtonDown(1))
            ShowRange();
        else if (Input.GetMouseButtonDown(2)) {
            if (Character.selectedCharacter != null) {
                if (Character.selectedCharacter.ShowingAttackRange)
                    Character.selectedCharacter.GetSelected();
                else
                    Character.selectedCharacter.ShowAttackRange();
            }
        }
    }

    void OnAxisFire1Down()
    {
        if (axisFire1Down != null)
            axisFire1Down();
    }

    void OnDestroy()
    {
        InputSignals.OnAxisFire1Down -= OnAxisFire1Down;
    }

    void ShowRange()
    {
        RaycastHit raycastHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out raycastHit)) {
            var target = raycastHit.collider.gameObject;
            if (target.CompareTag("Tile")) {
                if (Character.selectedCharacter != null)
                    Character.selectedCharacter.GetDeselected();
                Tile.Restore();

                var tile = target.GetComponent<Tile>();
                if (tile.Mark == Tile.EnumMarkType.walkable) {
                    ManageTerrain.Ins.reachableGraph.BFSearch(tile.reachableNode, ManageTerrain.Ins.defaultRange, (AdjacencyListNode<Tile> _node) => {
                        _node.mVal.GetHighlighted(Tile.EnumHighlightMode.source);
                    }, (AdjacencyListNode<Tile> _node) => {
                        _node.mVal.GetHighlighted(Tile.EnumHighlightMode.reachable);
                    });
                }
            }
        }
    }
}
