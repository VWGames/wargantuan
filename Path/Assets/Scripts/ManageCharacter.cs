﻿using System.Collections.Generic;
using UnityEngine;

public class ManageCharacter : MonoBehaviour
{
    public static ManageCharacter Ins { get; private set; }

    public GameObject prefabCoalossusSwordman;
    public GameObject prefabCoalossusSpearman;
    public GameObject prefabCoalossusBowman;
    public GameObject prefabCoalossusCavalry;
    public GameObject prefabTreehemothSwordman;
    public GameObject prefabTreehemothSpearman;
    public GameObject prefabTreehemothBowman;
    public GameObject prefabTreehemothCavalry;
    public GameObject prefabGraviathanSwordman;
    public GameObject prefabGraviathanSpearman;
    public GameObject prefabGraviathanBowman;
    public GameObject prefabGraviathanCavalry;

    public GameObject CurrentPrefabCharacter { get; set; }

    void Awake()
    {
        Ins = this;
    }

    public void SpawnCharacter()
    {
        if (CurrentPrefabCharacter == null) return;

        RaycastHit raycastHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out raycastHit)) {
            var target = raycastHit.collider.gameObject;
            if (target.CompareTag("Tile")) {
                var tile = target.GetComponent<Tile>();
                if (tile.Mark != Tile.EnumMarkType.nonWalkable) {
                    var character = Instantiate(CurrentPrefabCharacter).GetComponent<Character>();
                    GameEventSignals.DoCharacterSpawned(character, tile);
                }
            }
        }
    }

    public void ControlCharacter()
    {
        RaycastHit raycastHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out raycastHit)) {
            var target = raycastHit.collider.gameObject;
            if (target.CompareTag("Character")) {
                var character = target.GetComponent<Character>();
                if (Character.selectedCharacter != null
                    && Character.selectedCharacter.ShowingAttackRange && character.attachedTile.Attackable
                    && Character.selectedCharacter.faction != character.faction) {
                    Character.selectedCharacter.Target = character;
                    if (Character.TargetChanged) return;
                    Character.selectedCharacter.Attack(character);
                }
                else {
                    if (!character.Selectable) return;
                    character.GetSelected();
                    ManageUI.Ins.OnMoveModeButtonClick();
                }
            }
            else if (target.CompareTag("Tile")) {
                if (Character.selectedCharacter == null || Character.movingCharacter != null) return;

                var tile = target.GetComponent<Tile>();
                if (!tile.Reachable) {
                    if (Character.selectedCharacter != null)
                        Character.selectedCharacter.GetDeselected();
                    ManageUI.Ins.DeactivateButtonGroup1();
                    return;
                }

                tile.Selected = true;
                if (Tile.SelectedTileChanged) return;

                var selectedCharacter = Character.GetSelectedInstance();
                if (selectedCharacter != null)
                    selectedCharacter.MoveTo(tile);
                Tile.UnhighlightAll(Tile.EnumHighlightMode.reachable);
                GameEventSignals.DoTileSelected(tile, true);
                ManageUI.Ins.DeactivateButtonGroup1();
            }
        }// if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out raycastHit))
    }
}
