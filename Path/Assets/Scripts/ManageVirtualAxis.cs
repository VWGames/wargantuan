﻿using System.Collections.Generic;
using UnityEngine;

public class ManageVirtualAxis : MonoBehaviour
{
    public static ManageVirtualAxis Ins { get; private set; }

    //bool axisFire1Pressed = false;
    static Dictionary<string, bool> axisPressedMap = new Dictionary<string, bool>();
    static Dictionary<string, bool> axisReleasedMap = new Dictionary<string, bool>();

    void Awake()
    {
        if (!InitGlobalIns()) return;

        axisPressedMap["Fire1"] = false;
        axisPressedMap["Fire2"] = false;
        axisPressedMap["Fire3"] = false;

        axisReleasedMap["Fire1"] = false;
        axisReleasedMap["Fire2"] = false;
        axisReleasedMap["Fire3"] = false;
    }

    /*void Update()
    {
        if (Input.GetAxis("Fire1") != 0) {
            if (!axisFire1Pressed) {
                axisFire1Pressed = true;
                InputSignals.DoAxisFire1Down();
            }
        }
        else
            axisFire1Pressed = false;
    }*/

    public static bool GetAxis(string _axisName)
    {
        if (Input.GetAxis(_axisName) != 0)
            return true;
        return false;
    }

    public static bool GetAxisDown(string _axisName)
    {
        if (Input.GetAxis(_axisName) != 0) {
            if (axisPressedMap.ContainsKey(_axisName)) {
                if (!axisPressedMap[_axisName]) {
                    axisPressedMap[_axisName] = true;
                    return true;
                }
            }
        }
        else
            axisPressedMap[_axisName] = false;
        return false;
    }

    public static bool GetAxisUp(string _axisName)
    {
        if (!GetAxis(_axisName)) {
            if (axisReleasedMap[_axisName]) {
                axisReleasedMap[_axisName] = false;
                return true;
            }
            return false;
        }
        if (!axisReleasedMap[_axisName])
            axisReleasedMap[_axisName] = true;
        return false;
    }

    bool InitGlobalIns() {
        if (Ins == null)
            Ins = this;
        else if (Ins != this) {
            DestroyImmediate(gameObject);
            return false;
        }
        DontDestroyOnLoad(gameObject);
        return true;
    }
}
