﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{
    const int LARGE_WEIGHT = 10000;

    public static Color ModifyAlpha(Color _color, float _alpha)
    {
        _color.a = _alpha;
        return _color;
    }

    public static void ModifyAlpha(Renderer _renderer, float _alpha)
    {
        _renderer.material.color = ModifyAlpha(_renderer.material.color, _alpha);
    }

    public static HashSet<Character> GetFaction(Character _character)
    {
        switch (_character.faction) {
            case Character.EnumFaction.firefolk:
                return Character.firefolkFaction;
            case Character.EnumFaction.treefolk:
                return Character.treefolkFaction;
            case Character.EnumFaction.undead:
                return Character.undeadFaction;
            default:
                return null;
        }
    }

    public static Character.EnumFaction GetActiveFaction()
    {
        var activeFaction = Character.allFactions[Character.activeFactionIndex];
        var activeFactionInstanceEnumerator = activeFaction.GetEnumerator();
        activeFactionInstanceEnumerator.MoveNext();
        return activeFactionInstanceEnumerator.Current.faction;
    }

    public static string GetFactionName(Character.EnumFaction _faction)
    {
        switch (_faction) {
            case Character.EnumFaction.firefolk:
                return "FIREFOLK";
            case Character.EnumFaction.treefolk:
                return "TREEFOLK";
            case Character.EnumFaction.undead:
                return "UNDEAD";
            default:
                return string.Empty;
        }
    }

    public static Color GetFactionColor(Character.EnumFaction _faction)
    {
        switch (_faction) {
            case Character.EnumFaction.firefolk:
                return Color.red;
            case Character.EnumFaction.treefolk:
                return Color.green;
            case Character.EnumFaction.undead:
                return Color.white;
            default:
                return Color.cyan;
        }
    }

    public static Character.EnumFlankingState GetFlankingState(Character _defender)
    {
        int dir = 0;
        for (; dir < 4; ++dir)
            if (_defender.attachedTile.adjacentTiles[dir] == _defender.facingTile)
                break;
        
        if (_defender.attachedTile.adjacentTiles[dir] == _defender.attachedTile.attackableNode.mPredecessor.mVal)
            return Character.EnumFlankingState.front;
        if (_defender.attachedTile.adjacentTiles[(dir + 1) % 4] == _defender.attachedTile.attackableNode.mPredecessor.mVal
            || _defender.attachedTile.adjacentTiles[(dir + 3) % 4] == _defender.attachedTile.attackableNode.mPredecessor.mVal)
            return Character.EnumFlankingState.side;
        if (_defender.attachedTile.adjacentTiles[(dir + 2) % 4] == _defender.attachedTile.attackableNode.mPredecessor.mVal)
            return Character.EnumFlankingState.back;
        return Character.EnumFlankingState.front;
    }

    public static float GetFlankingDamage(Character.EnumFlankingState _flankingState)
    {
        switch (_flankingState) {
            case Character.EnumFlankingState.front:
                return 0;
            case Character.EnumFlankingState.side:
                return 1;
            case Character.EnumFlankingState.back:
                return 3;
        }
        return 0;
    }

    public static float GetTypeSuperiorityModifier(Character _attacker, Character _defender)
    {
        if (_attacker.type == Character.EnumType.spearman) {
            if (_defender.type == Character.EnumType.cavalry)
                return 3;
        }
        else if (_attacker.type == Character.EnumType.cavalry) {
            if (_defender.type == Character.EnumType.spearman)
                return -2;
            if (_defender.type == Character.EnumType.swordman
                || _defender.type == Character.EnumType.bowman)
                return 2;
        }
        else if (_attacker.type == Character.EnumType.bowman) {
            if (_defender.type == Character.EnumType.swordman 
                ||  _defender.type == Character.EnumType.spearman)
                return 2;
        }
        return 0;
    }

    public static int GetPushingDistance(Character.EnumType _type)
    {
        switch (_type) {
            case Character.EnumType.swordman:
                return 1;
            case Character.EnumType.spearman:
                return 2;
            case Character.EnumType.cavalry:
                return 4;
        }
        return 0;
    }

    public static Tile.EnumMarkType GetTileMark(string _mark)
    {
        switch (_mark) {
            case "1":
                return Tile.EnumMarkType.walkable;
            case "S":
                return Tile.EnumMarkType.swamp;
            case "F":
                return Tile.EnumMarkType.forest;
            case "W":
                return Tile.EnumMarkType.nonWalkable;
            case "H":
                return Tile.EnumMarkType.nonWalkable;
            case "D":
                return Tile.EnumMarkType.walkable;
            case "R":
                return Tile.EnumMarkType.walkable;
            case "V":
                return Tile.EnumMarkType.walkable;
            case "X":
                return Tile.EnumMarkType.nonWalkable;
            case "%":
                return Tile.EnumMarkType.hole;
            case "":
                return Tile.EnumMarkType.walkable;
        }
        return Tile.EnumMarkType.walkable;
    }

    public static int GetReachableTileWeight(Tile.EnumMarkType _mark)
    {
        switch (_mark) {
            case Tile.EnumMarkType.nonWalkable:
                return -1;
            case Tile.EnumMarkType.swamp:
                return 2;
            case Tile.EnumMarkType.hole:
                return LARGE_WEIGHT;
        }
        return 1;
    }

    public static int GetAttackableTileWeight(Tile.EnumMarkType _mark)
    {
        switch (_mark) {
            /*case Tile.EnumMarkType.forest:
                return -1;*/
            default:
                return 1;
        }
    }

    public static Character GetTileOccupant(Tile _tile)
    {
        if (_tile.reachableNode.mOccupied) {
            foreach (var elem in Character.all)
                if (elem.attachedTile == _tile)
                    return elem;
        }
        return null;
    }

    public static IEnumerator DelayAction<T>(Func<T> _func, Func<bool> _predicate)
    {
        while (!_predicate())
            yield return null;
        _func();
        yield return null;
    }

    public static IEnumerator DelayAction<T>(Func<T> _func, int _step)
    {
        while (_step > 0) {
            --_step;
            yield return null;
        }
        _func();
        yield return null;
    }
}
