﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using CCKit;

public class ManageTerrain : MonoBehaviour
{
    public static ManageTerrain Ins { get; private set; }

    public GameObject prefabGrassTile;
    public GameObject prefabSwampTile;
    public GameObject prefabForestTile;
    public GameObject prefabWaterSourceTile;
    public GameObject prefabHillTile;
    public GameObject prefabDesertTile;
    public GameObject prefabShallowWaterTile;
    public GameObject prefabVolcanicTile;
    public GameObject prefabLavaTile;
    public GameObject prefabHoleTile;

    public GameObject prefabTile;

    public AdjacencyList<Tile> reachableGraph = new AdjacencyList<Tile>();
    public AdjacencyList<Tile> attackableGraph = new AdjacencyList<Tile>();
    public AdjacencyList<Tile> graph = new AdjacencyList<Tile>();
    List<List<AdjacencyListNode<Tile>>> reachableNodes = new List<List<AdjacencyListNode<Tile>>>();
    List<List<AdjacencyListNode<Tile>>> attackableNodes = new List<List<AdjacencyListNode<Tile>>>();
    List<List<AdjacencyListNode<Tile>>> nodes = new List<List<AdjacencyListNode<Tile>>>();

    public int defaultRange = 5;
    public float gridYPos = 0.0f;
    [Tooltip("the separation between adjacent tiles")]
    [SerializeField] float separation;

    [HideInInspector] public float rowLength;
    [HideInInspector] public float colLength;
    delegate void SetEdgeDel(AdjacencyListNode<Tile> _head, AdjacencyListNode<Tile> _tail);

    void Awake()
    {
        Ins = this;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
    {
        int levelIndex = _scene.buildIndex;
        GenerateTerrain(levelIndex);
        GameEventSignals.DoTerrainGenerated();
    }

    void GenerateTerrain(int _levelIndex)
    {
        CsvParser parser = new CsvParser();
		TextAsset fileTextAsset = (TextAsset)Resources.Load(string.Format("Level Files/level_{0}", _levelIndex - 2), typeof(TextAsset));
		parser.Parse(fileTextAsset.text);
        //parser.Parse(string.Format("{0}/Level Files/level_{1}.csv", Application.streamingAssetsPath, _levelIndex));

        int rows = parser.ProcessedData.Count, cols = 0, largestCols = 0;
        GameObject currentPrefabTile = null;
        for (int row = 0; row < rows; ++row) {
            reachableNodes.Add(new List<AdjacencyListNode<Tile>>());
            attackableNodes.Add(new List<AdjacencyListNode<Tile>>());
            nodes.Add(new List<AdjacencyListNode<Tile>>());

            cols = parser.ProcessedData[row].Count;
            for (int col = 0; col < cols; ++col) {
                var currentRow = parser.ProcessedData[row];
                switch (currentRow[col]) {
                    case "1":
                        currentPrefabTile = prefabGrassTile;
                        break;
                    case "S":
                        currentPrefabTile = prefabSwampTile;
                        break;
                    case "F":
                        currentPrefabTile = prefabForestTile;
                        break;
                    case "W":
                        currentPrefabTile = prefabWaterSourceTile;
                        break;
                    case "H":
                        currentPrefabTile = prefabHillTile;
                        break;
                    case "D":
                        currentPrefabTile = prefabDesertTile;
                        break;
                    case "R":
                        currentPrefabTile = prefabShallowWaterTile;
                        break;
                    case "V":
                        currentPrefabTile = prefabVolcanicTile;
                        break;
                    case "X":
                        currentPrefabTile = prefabLavaTile;
                        break;
                    case "%":
                        currentPrefabTile = prefabHoleTile;
                        break;

                    case "":
                        currentPrefabTile = prefabTile;
                        break;
                    default:
                        currentPrefabTile = prefabTile;
                        break;
                }

                var tile = Instantiate(currentPrefabTile, new Vector3(separation * col, gridYPos, -separation * row), prefabTile.transform.rotation).GetComponent<Tile>();
                tile.Mark = Utility.GetTileMark(currentRow[col]);
                reachableNodes[row].Add(tile.reachableNode = reachableGraph.AddVertex(tile));
                attackableNodes[row].Add(tile.attackableNode = attackableGraph.AddVertex(tile));
                nodes[row].Add(tile.node = graph.AddVertex(tile));
            }
            if (largestCols < cols)
                largestCols = cols;
        }

        for (int row = 0; row < rows; ++row) {
            for (int col = reachableNodes[row].Count; col < cols; ++col) {
                var tile = Instantiate(prefabTile, new Vector3(separation * col, gridYPos, -separation * row), prefabTile.transform.rotation).GetComponent<Tile>();
                tile.Mark = Tile.EnumMarkType.nonWalkable;
                reachableNodes[row].Add(tile.reachableNode = reachableGraph.AddVertex(tile));
                attackableNodes[row].Add(tile.attackableNode = attackableGraph.AddVertex(tile));
                nodes[row].Add(tile.node = graph.AddVertex(tile));
            }
        }

        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                SetEdgeDel ReachableSetEdge = (AdjacencyListNode<Tile> _head, AdjacencyListNode<Tile> _tail) => {
                    reachableGraph.SetEdge(_head, _tail, Utility.GetReachableTileWeight(_tail.mVal.Mark));
                };
                SetEdgeDel AttackableSetEdge = (AdjacencyListNode<Tile> _head, AdjacencyListNode<Tile> _tail) => {
                    attackableGraph.SetEdge(_head, _tail, Utility.GetAttackableTileWeight(_tail.mVal.Mark));
                };
                SetEdgeDel SetEdge = (AdjacencyListNode<Tile> _head, AdjacencyListNode<Tile> _tail) => {
                    graph.SetEdge(_head, _tail, 1);
                };

                for (int ndx = nodes[i][j].mVal.adjacentTiles.Length - 1; ndx >= 0; --ndx)
                    nodes[i][j].mVal.adjacentTiles[ndx] = null;

                if (i != 0) {
                    ReachableSetEdge(reachableNodes[i][j], reachableNodes[i - 1][j]);
                    AttackableSetEdge(attackableNodes[i][j], attackableNodes[i - 1][j]);
                    SetEdge(nodes[i][j], nodes[i - 1][j]);
                    nodes[i][j].mVal.adjacentTiles[3] = nodes[i - 1][j].mVal;
                }
                if (i != rows - 1) {
                    ReachableSetEdge(reachableNodes[i][j], reachableNodes[i + 1][j]);
                    AttackableSetEdge(attackableNodes[i][j], attackableNodes[i + 1][j]);
                    SetEdge(nodes[i][j], nodes[i + 1][j]);
                    nodes[i][j].mVal.adjacentTiles[1] = nodes[i + 1][j].mVal;
                }
                if (j != 0) {
                    ReachableSetEdge(reachableNodes[i][j], reachableNodes[i][j - 1]);
                    AttackableSetEdge(attackableNodes[i][j], attackableNodes[i][j - 1]);
                    SetEdge(nodes[i][j], nodes[i][j - 1]);
                    nodes[i][j].mVal.adjacentTiles[2] = nodes[i][j - 1].mVal;
                }
                if (j != cols - 1) {
                    ReachableSetEdge(reachableNodes[i][j], reachableNodes[i][j + 1]);
                    AttackableSetEdge(attackableNodes[i][j], attackableNodes[i][j + 1]);
                    SetEdge(nodes[i][j], nodes[i][j + 1]);
                    nodes[i][j].mVal.adjacentTiles[0] = nodes[i][j + 1].mVal;
                }
            }
        }
        
        var tileRenderer = prefabTile.GetComponent<MeshRenderer>();
        rowLength = (cols - 1) * (separation - tileRenderer.bounds.extents.x) + cols * tileRenderer.bounds.extents.x;
        colLength = (rows - 1) * (separation - tileRenderer.bounds.extents.z) + rows * tileRenderer.bounds.extents.z;
    }

    /*void GenerateTerrain(int _levelIndex)
    {
        int rows = 0, cols = 0;

        string[] lines = System.IO.File.ReadAllLines(string.Format("{0}/Level Files/level_{1}.csv", Application.streamingAssetsPath, _levelIndex));
        rows = lines.Length;
        for (int row = 0, cols0 = 0; row < rows; ++row, cols0 = 0) {
            nodes.Add(new List<AdjacencyListNode<Tile>>());

            var stringReader = new System.IO.StringReader(lines[row]);
            int currentChar, mark = 0;
            GameObject currentPrefabTile = null;
            while ((currentChar = stringReader.Peek()) != -1) {
                stringReader.Read();
                if ((char)currentChar == ',') {
                    if (mark == 0) {
                        currentPrefabTile = prefabTile;
                        goto Proc0;
                    }
                    mark = 0;
                    continue;
                }
                else if ((char)currentChar == 'X') {
                    if (mark == 0) {
                        mark = 1;
                        currentPrefabTile = prefabTile2;
                    }
                }
                else if ((char)currentChar == 'E') {
                    if (mark == 0) {
                        mark = 1;
                        currentPrefabTile = prefabTile3;
                    }
                }
                else {
                    if (mark == 0) {
                        mark = 1;
                        currentPrefabTile = prefabTile;
                    }
                }
            Proc0:
                var tile = Instantiate(currentPrefabTile, new Vector3(separation * cols0++, gridYPos, -separation * row), prefabTile.transform.rotation).GetComponent<Tile>();
                tile.Mark = Utility.GetTileMark((char)currentChar);
                nodes[row].Add(tile.node = graph.AddVertex(tile));
            }// while ((currentChar = stringReader.Peek()) != -1)

            if (cols < cols0)
                cols = cols0;
        }
        
        for (int row = 0; row < rows; ++row) {
            for (int col = nodes[row].Count; col < cols; ++col) {
                var tile = Instantiate(prefabTile2, new Vector3(separation * col, gridYPos, -separation * row), prefabTile.transform.rotation).GetComponent<Tile>();
                tile.Mark = Tile.EnumMarkType.nonWalkable;
                nodes[row].Add(tile.node = graph.AddVertex(tile));
            }
        }

        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                SetEdgeDel SetEdge = (AdjacencyListNode<Tile> _head, AdjacencyListNode<Tile> _tail) => {
                    graph.SetEdge(_head, _tail, Utility.GetTileWeight(_tail.mVal.Mark));
                };
                if (i != 0)
                    SetEdge(nodes[i][j], nodes[i - 1][j]);
                if (i != rows - 1)
                    SetEdge(nodes[i][j], nodes[i + 1][j]);
                if (j != 0)
                    SetEdge(nodes[i][j], nodes[i][j - 1]);
                if (j != cols - 1)
                    SetEdge(nodes[i][j], nodes[i][j + 1]);
            }
        }

        var tileRenderer = prefabTile.GetComponent<MeshRenderer>();
        rowLength = (cols - 1) * (separation - tileRenderer.bounds.extents.x) + cols * tileRenderer.bounds.extents.x;
        ColLength = (rows - 1) * (separation - tileRenderer.bounds.extents.z) + rows * tileRenderer.bounds.extents.z;
    }*/
}
