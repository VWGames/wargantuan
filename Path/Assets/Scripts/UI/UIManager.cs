﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Button Btn_Play;
    public Button Quit;
    public Button Credits;
    public Button Map1;
    public Button Map2;
    public Button Map3;
    public Button Map4;
    public Button Map5;
    public Button Map6;
    public Button Map7;
    public Button Map8;
    public Button Map9;
    public Button Map10;
    public Button Map11;
    public Button Back;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void OnPlay()
    {
        SceneManager.LoadScene("MapSelect");
    }

    public void OnQuit()
    {
        Application.Quit();
    }

    public void onBack()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OnMapSelected(int _index)
    {
        SceneManager.LoadSceneAsync(_index + 2);
    }

    public void OnCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    /*public void OnMap1()
    {
        SceneManager.LoadScene("Scene 0");
    }

    public void OnMap2()
    {
        SceneManager.LoadScene("Scene 1");
    }

    public void OnMap3()
    {
        SceneManager.LoadScene("Scene 2");
    }

    public void OnMap4()
    {
        SceneManager.LoadScene("Scene 3");
    }

    public void OnMap5()
    {
        SceneManager.LoadScene("Scene 4");
    }

    public void OnMap6()
    {
        SceneManager.LoadScene("Scene 5");
    }

    public void OnMap7()
    {
        SceneManager.LoadScene("Scene 6");
    }

    public void OnMap8()
    {
        SceneManager.LoadScene("Scene 7");
    }

    public void OnMap9()
    {
        SceneManager.LoadScene("Scene 8");
    }

    public void OnMap10()
    {
        SceneManager.LoadScene("Scene 9");
    }

    public void OnMap11()
    {
        SceneManager.LoadScene("Scene 10");
    }*/
}
