﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageMenuInput : MonoBehaviour
{
    Vector2 prevMousePos, currentMousePos, originalMousePos;
    [SerializeField] RectTransform[] uiGroupRectTransforms;
    [SerializeField] GameObject eventSystemGameObject;
    [SerializeField] GameObject endMarker;
    Vector2[] originalSizeDeltas;

    [SerializeField] float touchTime;
    float touchTimer = -1;
    float touchOffset = 30;

    void Start()
    {
        prevMousePos = currentMousePos = GetMousePos();

        originalSizeDeltas = new Vector2[uiGroupRectTransforms.Length];
        for (int i = 0; i < originalSizeDeltas.Length; ++i)
            originalSizeDeltas[i] = uiGroupRectTransforms[i].sizeDelta;
        //Debug.Log(endMarker.transform.position);
    }

    void Update()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android) {
            currentMousePos = GetTouchPos();

            if (ManageVirtualAxis.GetAxisDown("Fire1")) {
                originalMousePos = GetTouchPos();
                touchTimer = 0;
            }
            if (ManageVirtualAxis.GetAxis("Fire1"))
            {
                var newSizeDeltas = new Vector2[uiGroupRectTransforms.Length];
                for (int i = 0; i < uiGroupRectTransforms.Length; ++i)
                {
                    var delta = uiGroupRectTransforms[i].sizeDelta;
                    var minSizeDelta1 = originalSizeDeltas[i][1];
                    newSizeDeltas[i] = new Vector2(delta[0], Mathf.Max(delta[1] + (currentMousePos[1] - prevMousePos[1]), minSizeDelta1));
                }
                var theEndReached = (endMarker.transform.position[1] < Screen.height);
                for (int i = 0; i < uiGroupRectTransforms.Length; ++i)
                    uiGroupRectTransforms[i].sizeDelta = theEndReached ? newSizeDeltas[i] : originalSizeDeltas[i];
            }
            if (ManageVirtualAxis.GetAxisUp("Fire1"))
            {
                touchTimer = -1;
                eventSystemGameObject.SetActive(true);
            }

            if (touchTimer >= 0)
            {
                touchTimer += Time.deltaTime;
                if (touchTimer > touchTime
                    || Mathf.Abs(currentMousePos[0] - originalMousePos[0]) > touchOffset
                    || Mathf.Abs(currentMousePos[1] - originalMousePos[1]) > touchOffset)
                {
                    touchTimer = -1;
                    eventSystemGameObject.SetActive(false);
                }
            }

            prevMousePos = currentMousePos;

            return;
        }

        currentMousePos = GetMousePos();
        
        if (ManageVirtualAxis.GetAxisDown("Fire1")) {
            originalMousePos = GetMousePos();
            touchTimer = 0;
        }
        if (ManageVirtualAxis.GetAxis("Fire1")) {
            var newSizeDeltas = new Vector2[uiGroupRectTransforms.Length];
            for (int i = 0; i < uiGroupRectTransforms.Length; ++i) {
                var delta = uiGroupRectTransforms[i].sizeDelta;
                var minSizeDelta1 = originalSizeDeltas[i][1];
                newSizeDeltas[i] = new Vector2(delta[0], Mathf.Max(delta[1] + (currentMousePos[1] - prevMousePos[1]), minSizeDelta1));
            }
            var theEndReached = (endMarker.transform.position[1] < Screen.height);
            for (int i = 0; i < uiGroupRectTransforms.Length; ++i)
                uiGroupRectTransforms[i].sizeDelta = theEndReached ? newSizeDeltas[i] : originalSizeDeltas[i];
        }
        if (ManageVirtualAxis.GetAxisUp("Fire1")) {
            touchTimer = -1;
            eventSystemGameObject.SetActive(true);
        }

        if (touchTimer >= 0) {
            touchTimer += Time.deltaTime;
            if (touchTimer > touchTime 
                || Mathf.Abs(currentMousePos[0] - originalMousePos[0]) > touchOffset
                || Mathf.Abs(currentMousePos[1] - originalMousePos[1]) > touchOffset) {
                touchTimer = -1;
                eventSystemGameObject.SetActive(false);
            }
        }

        prevMousePos = currentMousePos;
    }

    Vector2 GetMousePos()
    {
        return new Vector2(Input.mousePosition[0], Input.mousePosition[1]);
    }

    Vector2 GetTouchPos()
    {
        if (Input.touchCount == 0)
            return Vector2.zero;
        return new Vector2(Input.GetTouch(0).position[0], Input.GetTouch(0).position[1]);
    }
}
