﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ManageUI : MonoBehaviour
{
    public static ManageUI Ins { get; private set; }

    public Button endTurnButton;
    public Button spawnFirefolkButton;
    public Button spawnTreefolkButton;
    public Button spawnUndeadButton;
    public Button manageCharacterButton;
    public Button attackModeButton;
    public Button moveModeButton;
    public Button setDirectionButton;
    public Button coalossusSwordmanButton;
    public Button coalossusSpearmanButton;
    public Button coalossusBowmanButton;
    public Button coalossusCavalryButton;
    public Button treehemothSwordmanButton;
    public Button treehemothSpearmanButton;
    public Button treehemothBowmanButton;
    public Button treehemothCavalryButton;
    public Button graviathanSwordmanButton;
    public Button graviathanSpearmanButton;
    public Button graviathanBowmanButton;
    public Button graviathanCavalryButton;

    public Text actionPointsText;
    public Text activeFactionText;
    public Text damageText;
    public Text flankingStateText;

    Stack<Button> buttonGroup0 = new Stack<Button>();
    Stack<Button> buttonGroup1 = new Stack<Button>();

    Stack<Button> firefolkCharacterButtonGroup = new Stack<Button>();
    Stack<Button> treefolkCharacterButtonGroup = new Stack<Button>();
    Stack<Button> undeadCharacterButtonGroup = new Stack<Button>();

    public float activeButtonAlpha = 0.5f;
    public float inactiveButtonAlpha = 0.25f;

    void Awake()
    {
        Ins = this;

        endTurnButton.onClick.AddListener(() => { GameEventSignals.DoCurrentTurnEnd(); });

        spawnFirefolkButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = null;
            ManageInput.Ins.axisFire1Down = ManageCharacter.Ins.SpawnCharacter;
            HideCharacterButtonGroups();
            foreach (var elem in firefolkCharacterButtonGroup)
                elem.gameObject.SetActive(true);
            DeactivateButtonGroup(firefolkCharacterButtonGroup);
            if (Character.selectedCharacter != null)
                Character.selectedCharacter.GetDeselected();
        });
        spawnTreefolkButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = null;
            ManageInput.Ins.axisFire1Down = ManageCharacter.Ins.SpawnCharacter;
            HideCharacterButtonGroups();
            foreach (var elem in treefolkCharacterButtonGroup)
                elem.gameObject.SetActive(true);
            DeactivateButtonGroup(treefolkCharacterButtonGroup);
            if (Character.selectedCharacter != null)
                Character.selectedCharacter.GetDeselected();
        });
        spawnUndeadButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = null;
            ManageInput.Ins.axisFire1Down = ManageCharacter.Ins.SpawnCharacter;
            HideCharacterButtonGroups();
            foreach (var elem in undeadCharacterButtonGroup)
                elem.gameObject.SetActive(true);
            DeactivateButtonGroup(undeadCharacterButtonGroup);
            if (Character.selectedCharacter != null)
                Character.selectedCharacter.GetDeselected();
        });
        manageCharacterButton.onClick.AddListener(() => {
            ManageInput.Ins.axisFire1Down = ManageCharacter.Ins.ControlCharacter;
            if (Character.selectedCharacter != null)
                Character.selectedCharacter.GetDeselected();
        });

        attackModeButton.onClick.AddListener(() => { Character.selectedCharacter.ShowAttackRange(); });
        moveModeButton.onClick.AddListener(() => { Character.selectedCharacter.GetSelected(); });

        coalossusSwordmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabCoalossusSwordman;
        });
        coalossusSpearmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabCoalossusSpearman;
        });
        coalossusBowmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabCoalossusBowman;
        });
        coalossusCavalryButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabCoalossusCavalry;
        });
        treehemothSwordmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabTreehemothSwordman;
        });
        treehemothSpearmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabTreehemothSpearman;
        });
        treehemothBowmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabTreehemothBowman;
        });
        treehemothCavalryButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabTreehemothCavalry;
        });
        graviathanSwordmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabGraviathanSwordman;
        });
        graviathanSpearmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabGraviathanSpearman;
        });
        graviathanBowmanButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabGraviathanBowman;
        });
        graviathanCavalryButton.onClick.AddListener(() => {
            ManageCharacter.Ins.CurrentPrefabCharacter = ManageCharacter.Ins.prefabGraviathanCavalry;
        });

        GameEventSignals.OnCharacterSelected += OnCharacterSelected;
        GameEventSignals.OnActionPointsUpdated += OnActionPointsUpdated;
        GameEventSignals.OnTurnUpdated += OnTurnUpdated;
        GameEventSignals.OnCharacterTakeDamage += OnCharacterTakeDamage;

        buttonGroup0.Push(spawnFirefolkButton);
        buttonGroup0.Push(spawnTreefolkButton);
        buttonGroup0.Push(spawnUndeadButton);
        buttonGroup0.Push(manageCharacterButton);

        buttonGroup1.Push(attackModeButton);
        buttonGroup1.Push(moveModeButton);

        firefolkCharacterButtonGroup.Push(coalossusSwordmanButton);
        firefolkCharacterButtonGroup.Push(coalossusSpearmanButton);
        firefolkCharacterButtonGroup.Push(coalossusBowmanButton);
        firefolkCharacterButtonGroup.Push(coalossusCavalryButton);
        treefolkCharacterButtonGroup.Push(treehemothSwordmanButton);
        treefolkCharacterButtonGroup.Push(treehemothSpearmanButton);
        treefolkCharacterButtonGroup.Push(treehemothBowmanButton);
        treefolkCharacterButtonGroup.Push(treehemothCavalryButton);
        undeadCharacterButtonGroup.Push(graviathanSwordmanButton);
        undeadCharacterButtonGroup.Push(graviathanSpearmanButton);
        undeadCharacterButtonGroup.Push(graviathanBowmanButton);
        undeadCharacterButtonGroup.Push(graviathanCavalryButton);
    }

    void Start()
    {
        DeactivateButtonGroup0();
        DeactivateButtonGroup1();
        MakeNonInteractableButtonGroup1();
        HideCharacterButtonGroups();

        setDirectionButton.interactable = false;
    }

    void OnCharacterSelected(Character _character, bool _selected)
    {
        if (_selected) {
            actionPointsText.text = string.Format("{0} AP", _character.actionPoints);
            MakeInteractableButtonGroup1();
        }
        else {
            actionPointsText.text = string.Empty;
            MakeNonInteractableButtonGroup1();
        }
    }

    void OnActionPointsUpdated(Character _character)
    {
        if (Character.selectedCharacter == _character)
            actionPointsText.text = string.Format("{0} AP", _character.actionPoints);
    }

    void OnTurnUpdated(Character.EnumFaction _activeFaction)
    {
        activeFactionText.text = Utility.GetFactionName(_activeFaction);
        activeFactionText.color = Utility.GetFactionColor(_activeFaction);
    }

    void OnCharacterTakeDamage(Character _attacker, Character _defender, float _damage, Character.EnumFlankingState _flankingState)
    {
        damageText.text = string.Format("{0} damage", _damage);
        if (_flankingState == Character.EnumFlankingState.front)
            flankingStateText.text = "front attack";
        else if (_flankingState == Character.EnumFlankingState.side)
            flankingStateText.text = "side attack";
        else if (_flankingState == Character.EnumFlankingState.back)
            flankingStateText.text = "rear attack";

        var texts = new List<Text>() { damageText, flankingStateText };
        StartCoroutine(DelayRemoveTexts(texts, 300));
    }

    public void OnSpawnFirefolkButtonClick()
    {
        DeactivateButtonGroup0();
        var spawnFirefolkButtonImage = spawnFirefolkButton.GetComponent<Image>();
        spawnFirefolkButtonImage.color = Utility.ModifyAlpha(spawnFirefolkButtonImage.color, activeButtonAlpha);

        DeactivateButtonGroup1();
        MakeNonInteractableButtonGroup1();
    }

    public void OnSpawnTreefolkButtonClick()
    {
        DeactivateButtonGroup0();
        var spawnTreefolkButtonImage = spawnTreefolkButton.GetComponent<Image>();
        spawnTreefolkButtonImage.color = Utility.ModifyAlpha(spawnTreefolkButtonImage.color, activeButtonAlpha);

        DeactivateButtonGroup1();
        MakeNonInteractableButtonGroup1();
    }

    public void OnSpawnUndeadfolkButtonClick()
    {
        DeactivateButtonGroup0();
        var spawnUndeadButtonImage = spawnUndeadButton.GetComponent<Image>();
        spawnUndeadButtonImage.color = Utility.ModifyAlpha(spawnUndeadButtonImage.color, activeButtonAlpha);

        DeactivateButtonGroup1();
        MakeNonInteractableButtonGroup1();
    }

    public void OnManageCharacterButtonClick()
    {
        DeactivateButtonGroup0();
        var manageCharacterButtonImage = manageCharacterButton.GetComponent<Image>();
        manageCharacterButtonImage.color = Utility.ModifyAlpha(manageCharacterButtonImage.color, activeButtonAlpha);

        DeactivateButtonGroup1();
        HideCharacterButtonGroups();
    }

    public void OnAttackModeButtonClick()
    {
        DeactivateButtonGroup1();
        var attackModeButtonImage = attackModeButton.GetComponent<Image>();
        attackModeButtonImage.color = Utility.ModifyAlpha(attackModeButtonImage.color, activeButtonAlpha);
    }

    public void OnMoveModeButtonClick()
    {
        DeactivateButtonGroup1();
        var moveModeButtonImage = moveModeButton.GetComponent<Image>();
        moveModeButtonImage.color = Utility.ModifyAlpha(moveModeButtonImage.color, activeButtonAlpha);
    }

    public void OnSetDirectionButtonClick()
    {
        if (Character.selectedCharacter.directionButtonGroup.activeSelf)
            Character.selectedCharacter.directionButtonGroup.SetActive(false);
        else
            Character.selectedCharacter.directionButtonGroup.SetActive(true);
    }

    public void OnMainMenuButtonClick()
    {
        SceneManager.LoadScene(0);
    }

    public void OnCoalossusSwordmanButtonClick()
    {
        DeactivateButtonGroup(firefolkCharacterButtonGroup);
        var coalossusCharacterButtonImage = coalossusSwordmanButton.GetComponent<Image>();
        coalossusCharacterButtonImage.color = Utility.ModifyAlpha(coalossusCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnCoalossusSpearButtonClick()
    {
        DeactivateButtonGroup(firefolkCharacterButtonGroup);
        var coalossusCharacterButtonImage = coalossusSpearmanButton.GetComponent<Image>();
        coalossusCharacterButtonImage.color = Utility.ModifyAlpha(coalossusCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnCoalossusBowButtonClick()
    {
        DeactivateButtonGroup(firefolkCharacterButtonGroup);
        var coalossusCharacterButtonImage = coalossusBowmanButton.GetComponent<Image>();
        coalossusCharacterButtonImage.color = Utility.ModifyAlpha(coalossusCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnCoalossusCavalryButtonClick()
    {
        DeactivateButtonGroup(firefolkCharacterButtonGroup);
        var coalossusCharacterButtonImage = coalossusCavalryButton.GetComponent<Image>();
        coalossusCharacterButtonImage.color = Utility.ModifyAlpha(coalossusCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnTreehemothSwordmanButtonClick()
    {
        DeactivateButtonGroup(treefolkCharacterButtonGroup);
        var treehemothCharacterButtonImage = treehemothSwordmanButton.GetComponent<Image>();
        treehemothCharacterButtonImage.color = Utility.ModifyAlpha(treehemothCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnTreehemothSpearmanButtonClick()
    {
        DeactivateButtonGroup(treefolkCharacterButtonGroup);
        var treehemothCharacterButtonImage = treehemothSpearmanButton.GetComponent<Image>();
        treehemothCharacterButtonImage.color = Utility.ModifyAlpha(treehemothCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnTreehemothBowmanButtonClick()
    {
        DeactivateButtonGroup(treefolkCharacterButtonGroup);
        var treehemothCharacterButtonImage = treehemothBowmanButton.GetComponent<Image>();
        treehemothCharacterButtonImage.color = Utility.ModifyAlpha(treehemothCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnTreehemothCavalryButtonClick()
    {
        DeactivateButtonGroup(treefolkCharacterButtonGroup);
        var treehemothCharacterButtonImage = treehemothCavalryButton.GetComponent<Image>();
        treehemothCharacterButtonImage.color = Utility.ModifyAlpha(treehemothCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnGraviathanSwordmanButtonClick()
    {
        DeactivateButtonGroup(undeadCharacterButtonGroup);
        var graviathanCharacterButtonImage = graviathanSwordmanButton.GetComponent<Image>();
        graviathanCharacterButtonImage.color = Utility.ModifyAlpha(graviathanCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnGraviathanSpearmanButtonClick()
    {
        DeactivateButtonGroup(undeadCharacterButtonGroup);
        var graviathanCharacterButtonImage = graviathanSpearmanButton.GetComponent<Image>();
        graviathanCharacterButtonImage.color = Utility.ModifyAlpha(graviathanCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnGraviathanBowmanButtonClick()
    {
        DeactivateButtonGroup(undeadCharacterButtonGroup);
        var graviathanCharacterButtonImage = graviathanBowmanButton.GetComponent<Image>();
        graviathanCharacterButtonImage.color = Utility.ModifyAlpha(graviathanCharacterButtonImage.color, activeButtonAlpha);
    }

    public void OnGraviathanCavalryButtonClick()
    {
        DeactivateButtonGroup(undeadCharacterButtonGroup);
        var graviathanCharacterButtonImage = graviathanCavalryButton.GetComponent<Image>();
        graviathanCharacterButtonImage.color = Utility.ModifyAlpha(graviathanCharacterButtonImage.color, activeButtonAlpha);
    }

    void OnDestroy()
    {
        endTurnButton.onClick.RemoveAllListeners();
        spawnFirefolkButton.onClick.RemoveAllListeners();
        spawnTreefolkButton.onClick.RemoveAllListeners();
        spawnUndeadButton.onClick.RemoveAllListeners();
        manageCharacterButton.onClick.RemoveAllListeners();

        attackModeButton.onClick.RemoveAllListeners();
        moveModeButton.onClick.RemoveAllListeners();

        coalossusSwordmanButton.onClick.RemoveAllListeners();
        coalossusSpearmanButton.onClick.RemoveAllListeners();
        coalossusBowmanButton.onClick.RemoveAllListeners();
        coalossusCavalryButton.onClick.RemoveAllListeners();
        treehemothSwordmanButton.onClick.RemoveAllListeners();
        treehemothSpearmanButton.onClick.RemoveAllListeners();
        treehemothBowmanButton.onClick.RemoveAllListeners();
        treehemothCavalryButton.onClick.RemoveAllListeners();
        graviathanSwordmanButton.onClick.RemoveAllListeners();
        graviathanSpearmanButton.onClick.RemoveAllListeners();
        graviathanBowmanButton.onClick.RemoveAllListeners();
        graviathanCavalryButton.onClick.RemoveAllListeners();

        GameEventSignals.OnCharacterSelected -= OnCharacterSelected;
        GameEventSignals.OnActionPointsUpdated -= OnActionPointsUpdated;
        GameEventSignals.OnTurnUpdated -= OnTurnUpdated;
        GameEventSignals.OnCharacterTakeDamage -= OnCharacterTakeDamage;
    }

    void DeactivateButtonGroup0()
    {
        foreach (var elem in buttonGroup0) {
            var buttonImage = elem.GetComponent<Image>();
            buttonImage.color = Utility.ModifyAlpha(buttonImage.color, inactiveButtonAlpha);
        }
    }

    public void DeactivateButtonGroup1()
    {
        foreach (var elem in buttonGroup1) {
            var buttonImage = elem.GetComponent<Image>();
            buttonImage.color = Utility.ModifyAlpha(buttonImage.color, inactiveButtonAlpha);
        }
    }

    void DeactivateButtonGroup(Stack<Button> _buttonGroup)
    {
        foreach (var elem in _buttonGroup) {
            var buttonImage = elem.GetComponent<Image>();
            buttonImage.color = Utility.ModifyAlpha(buttonImage.color, inactiveButtonAlpha);
        }
    }

    void HideCharacterButtonGroups()
    {
        foreach (var elem in firefolkCharacterButtonGroup)
            elem.gameObject.SetActive(false);
        foreach (var elem in treefolkCharacterButtonGroup)
            elem.gameObject.SetActive(false);
        foreach (var elem in undeadCharacterButtonGroup)
            elem.gameObject.SetActive(false);
    }

    void MakeInteractableButtonGroup1()
    {
        foreach (var elem in buttonGroup1)
            elem.interactable = true;
    }

    void MakeNonInteractableButtonGroup1()
    {
        foreach (var elem in buttonGroup1)
            elem.interactable = false;
    }

    public static IEnumerator DelayRemoveTexts(List<Text> _texts, float _stepCount)
    {
        for (int step = 0; step < _stepCount; ++step)
            yield return null;
        foreach (var elem in _texts)
            elem.text = string.Empty;
        yield return null;
    }
}
