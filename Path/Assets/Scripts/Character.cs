﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CCKit;

public class Character : MonoBehaviour
{
    public enum EnumFaction { firefolk, treefolk, undead }
    public enum EnumType { swordman, spearman, bowman, cavalry }
    public enum EnumFlankingState { front, side, back }

    public static LinkedList<Character> all = new LinkedList<Character>();
    [HideInInspector]
    public Tile attachedTile;
    [HideInInspector]
    public Tile facingTile;

    public EnumFaction faction;
    public EnumType type;
    public float initialHealth;
    public int initialActionPoints = 7;
    public int attackRange = 1;
    public float armor;

    public float speed = 1.0f;
    public float selectedAlpha = 0.4f;
    public float deselectedAlpha = 1.0f;
    float highlightTime = 0.3f;
    public Text healthText;
    public Image attackableSignImage;
    public GameObject directionButtonGroup;

    public ParticleSystem runDustParticle;
    public ParticleSystem takeHitParticle;
    public ParticleSystem launchAttackParticle;
    public ParticleSystem perishParticle;

    float health;
    [HideInInspector]
    public int actionPoints;

    public Vector3 offset = Vector3.up;
    Stack<Tile> reachablePath = new Stack<Tile>();
    Vector3 reachableCurrent = Vector3.zero, reachableNext = Vector3.zero;
    Stack<Tile> attackablePath = new Stack<Tile>();
    Tile attackableCurrent = null, attackableNext = null;

    MeshRenderer[] meshRenderer;

    bool moving;
    public bool Moving
    {
        get { return moving; }
        set {
            if (moving = value)
                movingCharacter = this;
            else
                movingCharacter = null;
        }
    }

    bool attacking;
    public bool Attacking
    {
        get { return attacking; }
        set {
            if (attacking = value)
                attackingCharacter = this;
            else
                attackingCharacter = null;
        }
    }

    public bool BeingPushed { get; private set; }
    static Stack<Character> pusheeStack = new Stack<Character>();
    Tile pushingSourceTile = null;

    bool selected;
    public bool Selected
    {
        get { return selected; }
        private set {
            if (selected = value) {
                foreach (var elem in all)
                    if (elem != this) {
                        elem.selected = false;
                        elem.directionButtonGroup.SetActive(false);
                        OnCharacterSelected(this, false);
                    }
                GameEventSignals.DoCharacterSelected(selectedCharacter = this, true);
            }
            else {
                GameEventSignals.DoCharacterSelected(this, false);
                selectedCharacter = null;
            }
        }
    }

    public static bool TargetChanged { get; private set; }
    Character target = null;
    public Character Target
    {
        get { return target; }
        set {
            if (target == null || target != value) {
                TargetChanged = true;

                Tile.UnhighlightAll(Tile.EnumHighlightMode.attackablePath, Tile.EnumAlphaRestore.reachableAlpha);
                if (value != null)
                    value.attachedTile.GetHighlighted(Tile.EnumHighlightMode.attackablePath);
            }
            else
                TargetChanged = false;
            target = value;

            if (target != null) {
                var facingNode = target.attachedTile.attackableNode;
                while (facingNode.mPredecessor != attachedTile.attackableNode)
                    facingNode = facingNode.mPredecessor;
                transform.LookAt((facingTile = facingNode.mVal).transform.position + offset);
            }
        }
    }

    public bool Dead { get { return health <= 0.0f; } }

    public bool Selectable { get; set; }
    public bool ShowingAttackRange { get; private set; }
    public bool DirectionPicked { get; private set; }

    public static Character selectedCharacter = null;
    public static Character movingCharacter = null;
    public static Character attackingCharacter = null;

    public static HashSet<Character> firefolkFaction = new HashSet<Character>();
    public static HashSet<Character> treefolkFaction = new HashSet<Character>();
    public static HashSet<Character> undeadFaction = new HashSet<Character>();
    public static List<HashSet<Character>> allFactions = new List<HashSet<Character>>();
    public static int activeFactionIndex = 0;

    void Awake()
    {
        GameEventSignals.OnCharacterSelected += OnCharacterSelected;
        GameEventSignals.OnCharacterSpawned += OnCharacterSpawned;
        GameEventSignals.OnCharacterMovementStarted += OnCharacterMovementStarted;
        GameEventSignals.OnCharacterMovementCeased += OnCharacterMovementCeased;
        GameEventSignals.OnCharacterAttack += OnCharacterAttack;
        GameEventSignals.OnCharacterTakeDamage += OnCharacterTakeDamage;
        GameEventSignals.OnTurnUpdated += OnTurnUpdated;

        all.AddLast(this);
        Moving = false;
        Selectable = true;
        DirectionPicked = false;
        health = initialHealth;
        actionPoints = initialActionPoints;
        AddToFaction();

        meshRenderer = GetComponentsInChildren<MeshRenderer>();
        healthText.text = string.Format("{0}", health);
        attackableSignImage.enabled = false;
        directionButtonGroup.SetActive(false);
        gameObject.SetActive(false);
    }

    void Update()
    {
        HandleMovement();
        HandleAttackPathVisualization();
        HandlePushing();
    }

    void OnCharacterSelected(Character _character, bool _selected)
    {
        if (this == _character) {
            if (_selected == true)
                foreach (var elem in meshRenderer)
                    Utility.ModifyAlpha(elem, selectedAlpha);
            else
                foreach (var elem in meshRenderer)
                    Utility.ModifyAlpha(elem, deselectedAlpha);
        }
    }

    void OnCharacterSpawned(Character _character, Tile _tile)
    {
        if (this == _character) {
            attachedTile = _tile;
            attachedTile.reachableNode.mOccupied = true;
            attachedTile.attackableNode.mOccupied = true;
            transform.position = attachedTile.transform.position + offset;

            var begin = attachedTile.node.mAdjacencyMap.GetEnumerator();
            begin.MoveNext();
            facingTile = begin.Current.Key.mVal;

            transform.LookAt(facingTile.transform.position + offset);
            gameObject.SetActive(true);
        }
    }

    void OnCharacterMovementStarted(Character _character)
    {
        if (this == _character) {
            if (!runDustParticle.isPlaying)
                runDustParticle.Play();
        }
    }

    void OnCharacterMovementCeased(Character _character)
    {
        if (this == _character) {
            if (runDustParticle.isPlaying)
                runDustParticle.Stop();
            transform.LookAt(facingTile.transform.position + offset);
        }
    }

    void OnCharacterAttack(Character _attacker)
    {
        if (this == _attacker) {
            if (!launchAttackParticle.isPlaying)
                launchAttackParticle.Play();
        }
    }

    void OnCharacterTakeDamage(Character _attacker, Character _defender, float _damage, EnumFlankingState _flankingState)
    {
        if (this == _defender) {
            if (!takeHitParticle.isPlaying)
                takeHitParticle.Play();
            if (pusheeStack.Count > 0)
                pusheeStack.Pop().BeingPushed = true;
            TakeDamage(_damage);
            healthText.text = string.Format("{0}", health);
        }
    }

    void OnTurnUpdated(EnumFaction _activeFaction)
    {
        if (faction == _activeFaction)
            DirectionPicked = false;
    }

    public void OnDirectionButtonClick(int _dir)
    {
        DirectionPicked = true;
        switch (_dir) {
            case 0:
                facingTile = attachedTile.adjacentTiles[0];
                break;
            case 1:
                facingTile = attachedTile.adjacentTiles[1];
                break;
            case 2:
                facingTile = attachedTile.adjacentTiles[2];
                break;
            case 3:
                facingTile = attachedTile.adjacentTiles[3];
                break;
        }
        if (facingTile != null)
            transform.LookAt(facingTile.transform.position + offset);
    }

    void OnDestroy()
    {
        GameEventSignals.OnCharacterSelected -= OnCharacterSelected;
        GameEventSignals.OnCharacterSpawned -= OnCharacterSpawned;
        GameEventSignals.OnCharacterMovementStarted -= OnCharacterMovementStarted;
        GameEventSignals.OnCharacterMovementCeased -= OnCharacterMovementCeased;
        GameEventSignals.OnCharacterAttack -= OnCharacterAttack;
        GameEventSignals.OnCharacterTakeDamage -= OnCharacterTakeDamage;
        GameEventSignals.OnTurnUpdated -= OnTurnUpdated;

        if (!ManageAppState.AppIsClosing) {
            UnoccupyTile();
            all.Remove(this);
            var thisFaction = Utility.GetFaction(this);
            thisFaction.Remove(this);
            if (thisFaction.Count == 0) {
                allFactions.Remove(thisFaction);
                if (allFactions.Count > 0)
                    GameEventSignals.DoCurrentTurnEnd();
            }
        }
    }

    void HandleMovement()
    {
        if (Moving) {
            if (reachableCurrent == reachableNext) {
                if (reachablePath.Count == 0) {
                    Moving = false;
                    reachableCurrent = reachableNext = Vector3.zero;
                    GameEventSignals.DoSelectedTileReached(attachedTile);
                    GameEventSignals.DoCharacterMovementCeased(this);
                    return;
                }

                reachableCurrent = transform.position;
                reachableNext = reachablePath.Pop().transform.position + offset;
                transform.LookAt(reachableNext);
            }
            else {
                transform.position += transform.forward * (speed * Time.deltaTime);
                if (Vector3.Distance(reachableCurrent, reachableNext) < Vector3.Distance(reachableCurrent, transform.position))
                    transform.position = reachableCurrent = reachableNext;
            }
        }
    }

    float highlightTimer = 0;
    void HandleAttackPathVisualization()
    {
        if (Attacking) {
            if (attackableCurrent == attackableNext) {
                if (attackablePath.Count == 0) {
                    Attacking = false;
                    attackableCurrent = attackableNext = null;
                    if (actionPoints <= 0)
                        Tile.Restore();
                    return;
                }
                attackableNext = attackablePath.Pop();
                attackableNext.GetHighlighted(true);
            }
            else {
                highlightTimer += Time.deltaTime;
                if (highlightTimer > highlightTime) {
                    highlightTimer = 0;
                    
                    if ((attackableCurrent = attackableNext) != null)
                        attackableCurrent.GetHighlighted(false);
                }
            }
        }
    }

    void HandlePushing()
    {
        if (BeingPushed) {
            if (reachableCurrent == reachableNext) {
                if (reachablePath.Count == 0) {
                    BeingPushed = false;
                    if (attachedTile.Mark == Tile.EnumMarkType.hole)
                        Perish();
                    transform.LookAt(facingTile.transform.position + offset);
                    return;
                }
                if (reachablePath.Count == 1)
                    if (pusheeStack.Count > 0)
                        StartCoroutine(Utility.DelayAction(() => { pusheeStack.Pop().BeingPushed = true; return 1; }, 1));

                reachableCurrent = transform.position;
                reachableNext = reachablePath.Pop().transform.position + offset;
                transform.LookAt(reachableNext);
            }
            else {
                transform.position += transform.forward * (speed * Time.deltaTime);
                if (Vector3.Distance(reachableCurrent, reachableNext) < Vector3.Distance(reachableCurrent, transform.position))
                    transform.position = reachableCurrent = reachableNext;
            }
        }
    }

    public void GetSelected()
    {
        if (!Moving) {
            Tile.Restore();
            Selected = true;
            if (Target != null)
                Target = null;
            ShowingAttackRange = false;

            ManageTerrain.Ins.reachableGraph.Dijkstra(attachedTile.reachableNode, actionPoints
                , (AdjacencyListNode<Tile> _node) => {
                    _node.mVal.GetHighlighted(Tile.EnumHighlightMode.source);
                }, (AdjacencyListNode<Tile> _node) => {
                    _node.mVal.GetHighlighted(Tile.EnumHighlightMode.reachable);
                });

            if (!DirectionPicked)
                ManageUI.Ins.setDirectionButton.interactable = true;
        }
    }

    public void GetDeselected()
    {
        Tile.Restore();
        Selected = false;
        if (Target != null)
            Target = null;

        directionButtonGroup.SetActive(false);
        ManageUI.Ins.setDirectionButton.interactable = false;
    }

    public void ShowAttackRange()
    {
        if (!Moving && actionPoints > 0) {
            Tile.Restore();
            ShowingAttackRange = true;

            var attackableTiles = new Stack<Tile>();

            ManageTerrain.Ins.attackableGraph.BFSearch(attachedTile.attackableNode, attackRange
                , (AdjacencyListNode<Tile> _node) => { }
                , (AdjacencyListNode<Tile> _node) => { attackableTiles.Push(_node.mVal); }
                , (AdjacencyListNode<Tile> _node) => { return true; });

            foreach (var elem in attackableTiles) {
                // elem.attackableNode != attachedTile.attackableNode
                var currentNode = elem.attackableNode.mPredecessor;
                while (currentNode != attachedTile.attackableNode) {
                    if ((currentNode.mOccupied && elem.attackableNode.mDistance - currentNode.mDistance < 3)
                        || currentNode.mVal.Mark == Tile.EnumMarkType.forest)
                        goto Proc0;
                    currentNode = currentNode.mPredecessor;
                }
                elem.GetHighlighted(Tile.EnumHighlightMode.attackable);
            Proc0: { }
            }

            foreach (var elem in all)
                if (elem.attachedTile.Attackable && elem.faction != this.faction)
                    elem.attackableSignImage.enabled = true;
        }
    }

    /*public void ShowAttackRange()
    {
        if (!Moving) {
            Tile.Restore();
            ShowingAttackRange = true;

            var attackableTiles = new Stack<Tile>();

            ManageTerrain.Ins.attackableGraph.BFSearch(attachedTile.attackableNode, attackRange
                , (AdjacencyListNode<Tile> _node) => { }
                , (AdjacencyListNode<Tile> _node) => { attackableTiles.Push(_node.mVal); }
                , (AdjacencyListNode<Tile> _node) => { return true; });

            ManageTerrain.Ins.graph.BFSearch(attachedTile.node, attackRange
                , (AdjacencyListNode<Tile> _node) => {}
                , (AdjacencyListNode<Tile> _node) => {}
                , (AdjacencyListNode<Tile> _node) => { return true; });
            
            foreach (var elem in attackableTiles) {
                if (elem.attackableNode.mDistance == elem.node.mDistance) {
                    if (!elem.attackableNode.mOccupied) 
                        elem.GetHighlighted(Tile.EnumHighlightMode.attackable);
                    else {
                        // elem.attackableNode != attachedTile.attackableNode
                        var currentNode = elem.attackableNode.mPredecessor;
                        while (currentNode != attachedTile.attackableNode) {
                            if (currentNode.mOccupied)
                                goto Proc0;
                            currentNode = currentNode.mPredecessor;
                        }
                        elem.GetHighlighted(Tile.EnumHighlightMode.attackable);
                    }
                }
            Proc0: { }
            }

            foreach (var elem in all)
                if (elem.attachedTile.Attackable && elem.faction != this.faction)
                    elem.attackableSignImage.enabled = true;
        }
    }*/

    public void MoveTo(Tile _target)
    {
        if (_target.Reachable && !Moving) {
            Occupy(_target);

            var predecessorTile = attachedTile.reachableNode.mPredecessor.mVal;
            for (int i = 0; i < 4; ++i) {
                if (attachedTile.adjacentTiles[i] == predecessorTile) {
                    facingTile = attachedTile.adjacentTiles[(i + 2) % 4];
                    facingTile = (facingTile != null) ? facingTile : predecessorTile;
                    break;
                }
            }

            var currentNode = _target.reachableNode;
            while (currentNode.mDistance != 0) {
                reachablePath.Push(currentNode.mVal);
                currentNode = currentNode.mPredecessor;
            }
            Moving = true;

            var pathArray = reachablePath.ToArray();
            actionPoints -= pathArray[pathArray.Length - 1].reachableNode.mDistance;
            directionButtonGroup.SetActive(false);
            ManageUI.Ins.setDirectionButton.interactable = false;

            GameEventSignals.DoActionPointsUpdated(this);
            GameEventSignals.DoCharacterMovementStarted(this);
        }
    }

    public void Attack(Character _target)
    {
        if (actionPoints > 0) {
            var flankingState = Utility.GetFlankingState(_target);
            var damage = GetDamage(this, _target, flankingState);
            actionPoints = 0;

            attackablePath.Clear();
            var currentNode = _target.attachedTile.attackableNode;
            while (currentNode.mDistance != 0) {
                attackablePath.Push(currentNode.mVal);
                currentNode = currentNode.mPredecessor;
            }
            
            if (damage < _target.health)
                _target.GetPushed(_target.attachedTile.attackableNode.mPredecessor.mVal, this);

            Attacking = true;

            directionButtonGroup.SetActive(false);
            ManageUI.Ins.setDirectionButton.interactable = false;
            GameEventSignals.DoActionPointsUpdated(this);
            GameEventSignals.DoCharacterAttack(this);
            StartCoroutine(DelayInvokeCharacterTakeDamage(this, _target, damage, flankingState));
        }
    }

    // chain effect is enabled
    // variable pushing distance is enabled
    // intermediate points are NOT used to determine the pushing path
    /*bool GetPushed(Tile _pushingSourceTile, Character _pusher)
    {
        var pushingDist = Utility.GetPushingDistance(_pusher.type);
        if (pushingDist == 0) return false;

        pushingSourceTile = _pushingSourceTile;
        for (int i = 0; i < 4; ++i) {
            if (attachedTile.adjacentTiles[i] == pushingSourceTile) {
                var facingDir = (i + 2) % 4;
                var nextTile = attachedTile;

                for (int j = 0; j < pushingDist; ++j) {
                    var tempTile = nextTile;
                    nextTile = nextTile.adjacentTiles[facingDir];
                    if (nextTile == null 
                        || !ManageTerrain.Ins.reachableGraph.IsEdge(tempTile.reachableNode, nextTile.reachableNode)
                        || (nextTile.adjacentTiles[facingDir] == null && nextTile.reachableNode.mOccupied)) {
                        nextTile = tempTile;
                        break;
                    }
                    // nextTile != null
                    if (nextTile.reachableNode.mOccupied) break;
                }
                
                if (nextTile != attachedTile) {
                    var tileOccupant = Utility.GetTileOccupant(nextTile);
                    if (tileOccupant != null && !tileOccupant.GetPushed_0(nextTile.adjacentTiles[i], this))
                        if ((nextTile = nextTile.adjacentTiles[i]) == attachedTile)
                            break;
                    reachablePath.Push(nextTile);
                    Occupy(nextTile);
                    facingTile = attachedTile.adjacentTiles[facingDir];
                    facingTile = (facingTile != null) ? facingTile : attachedTile.adjacentTiles[i];
                    pusheeStack.Push(this);
                    return true;
                }
            }
        }
        return false;
    }*/

    // chain effect is enabled
    // variable pushing distance is enabled
    // intermediate points are used to determine the pushing path
    bool GetPushed(Tile _pushingSourceTile, Character _pusher)
    {
        var pushingDist = Utility.GetPushingDistance(_pusher.type);
        if (pushingDist == 0) return false;

        pushingSourceTile = _pushingSourceTile;
        for (int i = 0; i < 4; ++i) {
            if (attachedTile.adjacentTiles[i] == pushingSourceTile) {
                var facingDir = (i + 2) % 4;
                var nextTile = attachedTile;
                var pushingPath = new Stack<Tile>();

                for (int j = 0; j < pushingDist; ++j) {
                    var tempTile = nextTile;
                    nextTile = nextTile.adjacentTiles[facingDir];
                    if (nextTile == null 
                        || !ManageTerrain.Ins.reachableGraph.IsEdge(tempTile.reachableNode, nextTile.reachableNode)
                        || (nextTile.adjacentTiles[facingDir] == null && nextTile.reachableNode.mOccupied)) {
                        nextTile = tempTile;
                        break;
                    }
                    // nextTile != null
                    if (nextTile.reachableNode.mOccupied) {
                        pushingPath.Push(nextTile);
                        break;
                    }
                    pushingPath.Push(nextTile);
                }
                
                if (nextTile != attachedTile) { // pushingPath.Count > 0
                    var tileOccupant = Utility.GetTileOccupant(nextTile);

                    if (tileOccupant != null && !tileOccupant.GetPushed_0(nextTile.adjacentTiles[i], this)) 
                        if ((nextTile = pushingPath.Pop().adjacentTiles[i]) == attachedTile)
                            break;
                    while (pushingPath.Count > 0)
                        reachablePath.Push(pushingPath.Pop());

                    Occupy(nextTile);
                    facingTile = attachedTile.adjacentTiles[facingDir];
                    facingTile = (facingTile != null) ? facingTile : attachedTile.adjacentTiles[i];
                    pusheeStack.Push(this);
                    return true;
                }
            }
        }
        return false;
    }

    // chain effect is enabled
    // variable pushing distance is disabled
    bool GetPushed_0(Tile _pushingSourceTile, Character _pusher)
    {
        pushingSourceTile = _pushingSourceTile;
        for (int i = 0; i < 4; ++i) {
            if (attachedTile.adjacentTiles[i] == pushingSourceTile) {
                var facingDir = (i + 2) % 4;
                var nextTile = attachedTile.adjacentTiles[facingDir];
                if (nextTile == null) break;
                if (ManageTerrain.Ins.reachableGraph.IsEdge(attachedTile.reachableNode, nextTile.reachableNode)) {
                    var tileOccupant = Utility.GetTileOccupant(nextTile);
                    if (tileOccupant != null && !tileOccupant.GetPushed_0(attachedTile, this))
                        break;
                    reachablePath.Push(nextTile);
                    Occupy(nextTile);
                    facingTile = attachedTile.adjacentTiles[facingDir];
                    facingTile = (facingTile != null) ? facingTile : attachedTile.adjacentTiles[i];
                    pusheeStack.Push(this);
                    return true;
                }
            }
        }
        return false;
    }

    // chain effect is disabled
    /*void GetPushed(Tile _pushingSourceTile)
    {
        pushingSourceTile = _pushingSourceTile;
        for (int i = 0; i < 4; ++i) {
            if (attachedTile.adjacentTiles[i] == pushingSourceTile) {
                var facingDir = (i + 2) % 4;
                var nextTile = attachedTile.adjacentTiles[facingDir];
                if (nextTile == null) break;
                if (ManageTerrain.Ins.reachableGraph.IsEdge(attachedTile.reachableNode, nextTile.reachableNode)
                    && !nextTile.reachableNode.mOccupied) {
                    reachablePath.Push(nextTile);
                    Occupy(nextTile);
                    facingTile = attachedTile.adjacentTiles[facingDir];
                    BeingPushed = true;
                    break;
                }
            }
        }
    }*/

    static float GetDamage(Character _attacker, Character _defender, EnumFlankingState _flankingState)
    {
        var damage = Mathf.Ceil(_attacker.health * 0.1f) + Utility.GetFlankingDamage(_flankingState) + _attacker.actionPoints
            + Utility.GetTypeSuperiorityModifier(_attacker, _defender) - _defender.armor;
        return (damage > 0) ? damage : 1;
    }

    void TakeDamage(float _damage)
    {
        health -= _damage;
        if (Dead)
            Perish();
    }

    void Perish()
    {
        perishParticle.Play();
        StartCoroutine(Utility.DelayAction(() => { Destroy(gameObject); return 1; }
        , () => { return !perishParticle.isPlaying; }));
        GameEventSignals.DoCharacterPerish(this);
    }

    public static Character GetSelectedInstance()
    {
        foreach (var elem in all)
            if (elem.selected)
                return elem;
        return null;
    }

    void AddToFaction()
    {
        var currentFaction = Utility.GetFaction(this);
        if (currentFaction.Count == 0)
            allFactions.Add(currentFaction);
        currentFaction.Add(this);
        GameEventSignals.DoCurrentTurnEnd();
    }

    void Occupy(Tile _target)
    {
        attachedTile.reachableNode.mOccupied = false;
        attachedTile.attackableNode.mOccupied = false;
        _target.reachableNode.mOccupied = true;
        _target.attackableNode.mOccupied = true;
        attachedTile = _target;
    }

    void UnoccupyTile()
    {
        attachedTile.reachableNode.mOccupied = false;
        attachedTile.attackableNode.mOccupied = false;
        attachedTile = null;
    }

    public static void RestoreStaticFields()
    {
        all = new LinkedList<Character>();
        TargetChanged = false;
        selectedCharacter = null;
        movingCharacter = null;
        firefolkFaction = new HashSet<Character>();
        treefolkFaction = new HashSet<Character>();
        undeadFaction = new HashSet<Character>();
        allFactions = new List<HashSet<Character>>();
        activeFactionIndex = 0;
    }

    static IEnumerator DelayInvokeCharacterTakeDamage(Character _attacker, Character _defender, float _damage, EnumFlankingState _flankingState)
    {
        if (_attacker.Attacking) {
            while (true) {
                if (!_attacker.Attacking) {
                    GameEventSignals.DoCharacterTakeDamage(_attacker, _defender, _damage, _flankingState);
                    break;
                }
                yield return null;
            }
        }
        yield return null;
    }
}
