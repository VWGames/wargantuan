﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamRotation : MonoBehaviour {

    public GameObject Target;
    public float RotAngle = 0f;
    public float rSpeed = 1f;
    public float RotationAmount = 1.5f;

    public Button LeftButton;
    public Button RightButton;
	
	// Update is called once per frame
	void Update ()
    {  
        if(RotAngle != 0)
        {
            Rotate();
        }
	}

    public void OnLeftButton()
    {
        RotAngle -= 90f;   
    }

    public void OnRightButton()
    {
        RotAngle += 90f;  
    }

    void Rotate()
    {
        float step = rSpeed * Time.deltaTime;

        if(RotAngle > 0)
        {
            
            transform.RotateAround(Target.transform.position, Vector3.up, -RotationAmount);
            RotAngle -= RotationAmount;
        }

        else if(RotAngle < 0)
        {
            transform.RotateAround(Target.transform.position, Vector3.up, RotationAmount);
            RotAngle += RotationAmount;
        }
    }
}
