﻿using System.Collections.Generic;
using UnityEngine;
using CCKit;

public class Tile : MonoBehaviour
{
    public enum EnumMarkType { walkable, nonWalkable, swamp, forest, hole }
    public enum EnumAlphaRestore { defaultAlpha, reachableAlpha }
    public enum EnumHighlightMode { source, reachable, attackable, reachablePath, attackablePath }

    [SerializeField] float defaultAlpha = 1.0f;
    [SerializeField] float sourceAlpha = 0.5f;
    [SerializeField] float reachableAlpha = 0.2f;
    [SerializeField] float reachablePathAlpha = 0.5f;
    [SerializeField] float attackableAlpha = 0.0f;
    [SerializeField] float attackablePathAlpha = 0.5f;
    [SerializeField] float defaultMetallic = 0f;
    [SerializeField] float reachablePathMetallic = 1.0f;

    public AdjacencyListNode<Tile> reachableNode;
    public AdjacencyListNode<Tile> attackableNode;
    public AdjacencyListNode<Tile> node;
    public EnumMarkType Mark { get; set; }
    public bool Reachable { get; private set; }
    public bool Attackable { get; private set; }

    [HideInInspector] public Tile[] adjacentTiles = new Tile[4];

    public static bool SelectedTileChanged { get; private set; }
    bool selected;
    public bool Selected {
        get { return selected; }
        set {
            if (selected = value) {
                SelectedTileChanged = false;
                if (prevSelectedTile == null || prevSelectedTile != this) {
                    SelectedTileChanged = true;
                    if (prevSelectedTile != null)
                        prevSelectedTile.Selected = false;
                    GameEventSignals.DoTileSelected(prevSelectedTile, false);
                    GameEventSignals.DoTileSelected(this, true);
                }
                prevSelectedTile = this;
            }
        }
    }

    static Stack<Tile> reachableTiles = new Stack<Tile>();
    static Stack<Tile> reachablePathTiles = new Stack<Tile>();
    static Stack<Tile> attackableTiles = new Stack<Tile>();
    static Stack<Tile> attackablePathTiles = new Stack<Tile>();
    static Tile prevSelectedTile = null;

    public TextMesh actionPointsText;

    void Awake()
    {
        GameEventSignals.OnTileSelected += OnTileSelected;
        GameEventSignals.OnSelectedTileReached += OnSelectedTileReached;

        Selected = Reachable = false;
    }

    void OnTileSelected(Tile _tile, bool _selected)
    {
        if (this == _tile) {
            if (_selected) {
                actionPointsText.gameObject.SetActive(true);
                actionPointsText.text = reachableNode.mDistance.ToString();

                UnhighlightAll(EnumHighlightMode.reachablePath, EnumAlphaRestore.reachableAlpha);
                GetHighlighted(EnumHighlightMode.reachablePath);
            }
            else {
                actionPointsText.gameObject.SetActive(false);

                UnhighlightAll(EnumHighlightMode.reachablePath, EnumAlphaRestore.reachableAlpha);
            }
        }
    }

    void OnSelectedTileReached(Tile _tile)
    {
        if (this == _tile) {
            actionPointsText.gameObject.SetActive(false);
            UnhighlightAll(EnumHighlightMode.reachablePath, EnumAlphaRestore.defaultAlpha);
        }
    }

    void OnDestroy()
    {
        GameEventSignals.OnTileSelected -= OnTileSelected;
        GameEventSignals.OnSelectedTileReached -= OnSelectedTileReached;
    }

    public void GetHighlighted(EnumHighlightMode _mode = EnumHighlightMode.reachable)
    {
        if (_mode == EnumHighlightMode.reachable) {
            var tileRenderer = GetComponent<MeshRenderer>();
            Utility.ModifyAlpha(tileRenderer, reachableAlpha);
            Reachable = true;
            reachableTiles.Push(this);
        }
        else if (_mode == EnumHighlightMode.source) {
            var tileRenderer = GetComponent<MeshRenderer>();
            Utility.ModifyAlpha(tileRenderer, sourceAlpha);
            Reachable = true;
            reachableTiles.Push(this);
        }
        else if (_mode == EnumHighlightMode.reachablePath) {
            var currentNode = reachableNode;
            while (currentNode.mDistance > 0) {
                var currentTile = currentNode.mVal;
                var currentTileRenderer = currentTile.GetComponent<MeshRenderer>();
                Utility.ModifyAlpha(currentTileRenderer, reachablePathAlpha);
                currentTileRenderer.material.SetFloat("_Metallic", reachablePathMetallic);
                reachablePathTiles.Push(currentTile);
                currentNode = currentNode.mPredecessor;
            }
        }
        else if (_mode == EnumHighlightMode.attackable) {
            var tileRenderer = GetComponent<MeshRenderer>();
            Utility.ModifyAlpha(tileRenderer, attackableAlpha);
            Attackable = true;
            attackableTiles.Push(this);
        }
        else if (_mode == EnumHighlightMode.attackablePath) {
            var currentNode = attackableNode;
            while (currentNode.mDistance > 0) {
                var currentTile = currentNode.mVal;
                Utility.ModifyAlpha(currentTile.GetComponent<MeshRenderer>(), attackablePathAlpha);
                attackablePathTiles.Push(currentTile);
                currentNode = currentNode.mPredecessor;
            }
        }
    }

    public void GetHighlighted(bool _flag)
    {
        if (_flag)
            GetComponent<MeshRenderer>().material.SetFloat("_Metallic", reachablePathMetallic);
        else
            GetComponent<MeshRenderer>().material.SetFloat("_Metallic", defaultMetallic);
    }

    public static void UnhighlightAll(EnumHighlightMode _mode, EnumAlphaRestore _alpha = EnumAlphaRestore.defaultAlpha)
    {
        if (_mode == EnumHighlightMode.reachable) {
            while (reachableTiles.Count > 0) {
                var currentTile = reachableTiles.Pop();
                currentTile.Reachable = false;
                Utility.ModifyAlpha(currentTile.GetComponent<MeshRenderer>(), currentTile.defaultAlpha);
            }
        }
        else if (_mode == EnumHighlightMode.reachablePath) {
            if (_alpha == EnumAlphaRestore.reachableAlpha) {
                while (reachablePathTiles.Count > 0) {
                    var currentTile = reachablePathTiles.Pop();
                    var currentTileRenderer = currentTile.GetComponent<MeshRenderer>();
                    Utility.ModifyAlpha(currentTile.GetComponent<MeshRenderer>(), currentTile.reachableAlpha);
                    currentTileRenderer.material.SetFloat("_Metallic", currentTile.defaultMetallic);
                }
            }
            else if (_alpha == EnumAlphaRestore.defaultAlpha) {
                while (reachablePathTiles.Count > 0) {
                    var currentTile = reachablePathTiles.Pop();
                    var currentTileRenderer = currentTile.GetComponent<MeshRenderer>();
                    Utility.ModifyAlpha(currentTileRenderer, currentTile.defaultAlpha);
                    currentTileRenderer.material.SetFloat("_Metallic", currentTile.defaultMetallic);
                }
            }
        }
        else if (_mode == EnumHighlightMode.attackable) {
            while (attackableTiles.Count > 0) {
                var currentTile = attackableTiles.Pop();
                currentTile.Attackable = false;
                Utility.ModifyAlpha(currentTile.GetComponent<MeshRenderer>(), currentTile.defaultAlpha);
            }
        }
        else if (_mode == EnumHighlightMode.attackablePath) {
            if (_alpha == EnumAlphaRestore.reachableAlpha) {
                while (attackablePathTiles.Count > 0) {
                    var currentTile = attackablePathTiles.Pop();
                    Utility.ModifyAlpha(currentTile.GetComponent<MeshRenderer>(), currentTile.attackableAlpha);
                }
            }
            else if (_alpha == EnumAlphaRestore.defaultAlpha) {
                while (attackablePathTiles.Count > 0) {
                    var currentTile = attackablePathTiles.Pop();
                    Utility.ModifyAlpha(currentTile.GetComponent<MeshRenderer>(), currentTile.defaultAlpha);
                }
            }
        }
    }
    
    public static void Restore()
    {
        foreach (var elem in Character.all)
            elem.attackableSignImage.enabled = false;
        
        UnhighlightAll(EnumHighlightMode.reachable);
        UnhighlightAll(EnumHighlightMode.attackable);
        UnhighlightAll(EnumHighlightMode.reachablePath, EnumAlphaRestore.defaultAlpha);
        UnhighlightAll(EnumHighlightMode.attackablePath, EnumAlphaRestore.defaultAlpha);
        if (prevSelectedTile != null)
            prevSelectedTile.actionPointsText.gameObject.SetActive(false);
        prevSelectedTile = null;
    }

    public static void RestoreStaticFields()
    {
        SelectedTileChanged = false;
        reachableTiles = new Stack<Tile>();
        reachablePathTiles = new Stack<Tile>();
        attackableTiles = new Stack<Tile>();
        attackablePathTiles = new Stack<Tile>();
        prevSelectedTile = null;
    }
}
