﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookatScript : MonoBehaviour {

    public Transform Target;
    GameObject maincam;

	// Use this for initialization
	void Start ()
    {
        //add implementation of finding camera object in scene
        maincam = GameObject.FindGameObjectWithTag("MainCamera");
        Target = maincam.transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.LookAt(Target.position);
    }
}
