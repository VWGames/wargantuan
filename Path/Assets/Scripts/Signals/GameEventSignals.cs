﻿using UnityEngine;

public static class GameEventSignals
{
    /// <summary>
    /// pending
    /// </summary>
    public static event TileSelectedHandler OnTileSelected;
    public delegate void TileSelectedHandler(Tile _tile, bool _selected);
    public static void DoTileSelected(Tile _tile, bool _selected) { if (OnTileSelected != null) OnTileSelected(_tile, _selected); }

    /// <summary>
    /// pending
    /// </summary>
    public static event SelectedTileReachedHandler OnSelectedTileReached;
    public delegate void SelectedTileReachedHandler(Tile _tile);
    public static void DoSelectedTileReached(Tile _tile) { if (OnSelectedTileReached != null) OnSelectedTileReached(_tile); }

    /// <summary>
    /// pending
    /// </summary>
    public static event TerrainGeneratedHandler OnTerrainGenerated;
    public delegate void TerrainGeneratedHandler();
    public static void DoTerrainGenerated() { if (OnTerrainGenerated != null) OnTerrainGenerated(); }

    /// <summary>
    /// pending
    /// </summary>
    public static event CharacterSelectedHandler OnCharacterSelected;
    public delegate void CharacterSelectedHandler(Character _character, bool _selected);
    public static void DoCharacterSelected(Character _character, bool _selected) { if (OnCharacterSelected != null) OnCharacterSelected(_character, _selected); }

    /// <summary>
    /// pending
    /// </summary>
    public static event CharacterMovementStartedHandler OnCharacterMovementStarted;
    public delegate void CharacterMovementStartedHandler(Character _character);
    public static void DoCharacterMovementStarted(Character _character) { if (OnCharacterMovementStarted != null) OnCharacterMovementStarted(_character); }

    /// <summary>
    /// pending
    /// </summary>
    public static event CharacterMovementCeasedHandler OnCharacterMovementCeased;
    public delegate void CharacterMovementCeasedHandler(Character _character);
    public static void DoCharacterMovementCeased(Character _character) { if (OnCharacterMovementCeased != null) OnCharacterMovementCeased(_character); }

    /// <summary>
    /// pending
    /// </summary>
    public static event CharacterSpawnedHandler OnCharacterSpawned;
    public delegate void CharacterSpawnedHandler(Character _character, Tile _tile);
    public static void DoCharacterSpawned(Character _character, Tile _tile) { if (OnCharacterSpawned != null) OnCharacterSpawned(_character, _tile); }

    /// <summary>
    /// pending
    /// </summary>
    public static event CharacterTakeDamageHandler OnCharacterTakeDamage;
    public delegate void CharacterTakeDamageHandler(Character _attacker, Character _defender, float _damage, Character.EnumFlankingState _flankingState);
    public static void DoCharacterTakeDamage(Character _attacker, Character _defender, float _damage, Character.EnumFlankingState _flankingState)
    { if (OnCharacterTakeDamage != null) OnCharacterTakeDamage(_attacker, _defender, _damage, _flankingState); }

    /// <summary>
    /// pending
    /// </summary>
    public static event CharacterAttackHandler OnCharacterAttack;
    public delegate void CharacterAttackHandler(Character _attacker);
    public static void DoCharacterAttack(Character _attacker) { if (OnCharacterAttack != null) OnCharacterAttack(_attacker); }

    /// <summary>
    /// pending
    /// </summary>
    public static event CharacterPerishHandler OnCharacterPerish;
    public delegate void CharacterPerishHandler(Character _attacker);
    public static void DoCharacterPerish(Character _perisher) { if (OnCharacterPerish != null) OnCharacterPerish(_perisher); }

    /// <summary>
    /// pending
    /// </summary>
    public static event CurrentTurnEndHandler OnCurrentTurnEnd;
    public delegate void CurrentTurnEndHandler();
    public static void DoCurrentTurnEnd() { if (OnCurrentTurnEnd != null) OnCurrentTurnEnd(); }

    /// <summary>
    /// pending
    /// </summary>
    public static event TurnUpdatedHandler OnTurnUpdated;
    public delegate void TurnUpdatedHandler(Character.EnumFaction _activeFaction);
    public static void DoTurnUpdated(Character.EnumFaction _activeFaction) { if (OnTurnUpdated != null) OnTurnUpdated(_activeFaction); }

    /// <summary>
    /// pending
    /// </summary>
    public static event ActionPointsUpdatedHandler OnActionPointsUpdated;
    public delegate void ActionPointsUpdatedHandler(Character _character);
    public static void DoActionPointsUpdated(Character _character) { if (OnActionPointsUpdated != null) OnActionPointsUpdated(_character); }
}
