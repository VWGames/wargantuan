﻿using UnityEngine;

public class ManageAppState : MonoBehaviour
{
    public static bool AppIsClosing { get; private set; }

	void Awake()
    {
        AppIsClosing = false;
    }

    void OnApplicationQuit()
    {
        AppIsClosing = true;
    }

    void OnDestroy()
    {
        Tile.RestoreStaticFields();
        Character.RestoreStaticFields();
    }
}
