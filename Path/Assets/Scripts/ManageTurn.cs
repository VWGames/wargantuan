﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageTurn : MonoBehaviour
{
    void Awake()
    {
        GameEventSignals.OnCurrentTurnEnd += OnCurrentTurnEnd;
    }

    void OnDestroy()
    {
        GameEventSignals.OnCurrentTurnEnd -= OnCurrentTurnEnd;
    }

    void OnCurrentTurnEnd()
    {
        if (Character.allFactions.Count > 0) {
            EndCurrentTurn();
            GameEventSignals.DoTurnUpdated(Utility.GetActiveFaction());
        }
    }

    void EndCurrentTurn()
    {
        Character.activeFactionIndex = (Character.activeFactionIndex + 1) % Character.allFactions.Count;
        for (int i = Character.allFactions.Count - 1; i >= 0; --i) {
            if (i != Character.activeFactionIndex)
                foreach (var elem in Character.allFactions[i])
                    elem.Selectable = false;
            else {
                foreach (var elem in Character.allFactions[i]) {
                    elem.Selectable = true;
                    elem.actionPoints = elem.initialActionPoints;
                }
            }
        }

        if (Character.selectedCharacter != null)
            Character.selectedCharacter.GetDeselected();
    }
}
